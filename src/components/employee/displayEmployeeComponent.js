import React,{Component} from 'react';
import '../../App.css';

class DisplayEmployeeComponent extends Component
{

    render()
    {
        if(this.props.displayEmployeeComponent !== undefined)
        {
       
            return <table border="2">
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Qualification</th>
                        <th>Phone</th>
                    </tr>  
                    {this.props.displayEmployeeComponent.map((employee) => (               
                    <tr>
                        <td>{employee.name}</td>
                        <td>{employee.address}</td>
                        <td>{employee.qualification}</td>
                        <td>{employee.phone}</td>
                    </tr>
            ))}
            </table>
        }
        else
        {
            return null;
        }
    }
}
export default DisplayEmployeeComponent;