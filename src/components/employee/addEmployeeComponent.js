import React, { Component } from 'react';
import { withRouter } from 'react-router';
import 'react-datepicker/dist/react-datepicker.css';


class AddEmployeeComponent extends Component {
  constructor(props) {
    super(props);
    this.personalFormSubmitted=false;
}
  
  state =
    {
      id: '',
      name: '',
      address: '',
      phone: '',
      qualification: '',
      workProfile: [
        {
          id: '',
          designation: '',
          jobDescription: '',
          achievements: '',
          skills: '',
          fromDate: '',
          toDate: ''
        }
      ]
    }

    onSubmit = (e) => 
  {
    e.preventDefault();
      this.props.employeeComponent(this.state);
      this.setState
        (
          {
            [e.target.name]: '',
            [e.target.address]: '',
            [e.target.phone]: '',
            [e.target.qualification]: '',
            [e.target.workProfile]: ''
          }
        );
        this.props.history.push('/');
  }
  addClick() 
  {
      const item = 
      {
            id: '',
            designation: '',
            jobDescription: '',
            achievements: '',
            skills: '',
            fromDate: '',
            toDate: ''
      };
      this.setState
      ({
        workProfile: [...this.state.workProfile, item]
      });
  }
  onChange = (e) => this.setState
    (
      {
        [e.target.name]: e.target.value,
        [e.target.address]: e.target.value,
        [e.target.phone]: e.target.value,
        [e.target.qualification]: e.target.value
      }
    );
  createUI() 
  {
      return this.state.workProfile.map((el, i) =>
        <div key={i}>
        <tr>
          <td> Designation:<input type="text" name="designation" value={el.designation} placeholder="Designation" onChange={this.handleChange.bind(this, i)} /></td>
          <td> JobDescription:<textarea name="jobDescription" value={el.jobDescription} placeholder="JobDescription" onChange={this.handleChange.bind(this, i)} /></td>
          <td> Achievements:<textarea name="achievements" value={el.achievements} placeholder="Acheivements" onChange={this.handleChange.bind(this, i)} /></td>
          <td> From :<input type="date" name="fromDate" value={el.fromDate} placeholder="FromDate" onChange={this.handleChange.bind(this, i)} /></td>
          <td> To :<input type="date" name="toDate" value={el.toDate} placeholder="toDate" onChange={this.handleChange.bind(this, i)} /></td>
        </tr>
        </div>
      )
  }
  handleChange(i, e) 
  {
    const values = [...this.state.workProfile];
    console.log(JSON.stringify(values));
    if(e.target.name === "designation")
    {
      values[i].designation = e.target.value;
    }
    if(e.target.name === "jobDescription")
    {
      values[i].jobDescription = e.target.value;
    }
    if(e.target.name === "achievements")
    {
      values[i].achievements = e.target.value;
    }
    if(e.target.name === "fromDate")
    {
      values[i].fromDate = e.target.value;
    }
    this.setState({ workprofile: values });
  }
  render() 
  {
    return (
      <div className="add">
        <br />
        <form onSubmit={this.onSubmit}>
          <div className="employeeForm">
              Name: <input type="text" name="name" placeholder="Name of Employee"
                value={this.state.name} onChange={this.onChange} />
              <br />
              Address:   <input type="text" name="address" placeholder="Address" value={this.state.address} onChange={this.onChange} />
              <br />
              Phone: <input type="text" name="phone" placeholder="Phone" value={this.state.phone} onChange={this.onChange} />
              <br />
              Qualification: <input type="text" name="qualification" placeholder="qualification" value={this.state.qualification} onChange={this.onChange} />
              <br />
              <br />
            </div>
            <div className="workProfileForm">
              <table cellpadding="0" cellspacing="5">
              {this.createUI()}
              </table>
            </div>          
              <input type='button' className="button" value='Add Work Profile' onClick={this.addClick.bind(this)} />
              <input type="submit" className="button" value="Submit" />
        </form>
      </div>
    );
  }
}
export default withRouter(AddEmployeeComponent);