import React from 'react';

function Header()
{
    return(
        <header style={headerStyle}>
            <b><center>Employee Data Form</center></b>
        </header>
    )
}

const headerStyle={
    background:'#A9A9A9',
    height:'30px'
}

export default Header;