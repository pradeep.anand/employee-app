import React,{Component} from 'react';
import AddEmployeeComponent from './components/employee/addEmployeeComponent';
import DisplayEmployeeComponent from './components/employee/displayEmployeeComponent';
import Header from './components/layouts/Header';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './App.css';


class App extends Component
{
  state={
    employees:[]
  }
  employeeComponent = (employee) => 
  {
    const newEmployee={
      id:1,
      name:employee.name,
      address:employee.address,
      phone:employee.phone,
      qualification:employee.qualification,
      workProfile:employee.workProfile
    }
    this.setState({employees:[...this.state.employees,newEmployee]});
  }
  render()
  {
    return (
      <div className="App">
        <Header/>    
        <Router>
            <Switch>        
              <Route path="/addEmployeeComponent">
                <AddEmployeeComponent employeeComponent={this.employeeComponent} history={this.props.history}/>
              </Route>
              <Route path="/">
                <Link to="/addEmployeeComponent" className="button1" style={{float:"right"}}>Add Employee</Link>
                <DisplayEmployeeComponent displayEmployeeComponent={this.state.employees}/>
              </Route>
            </Switch>
         </Router> 
      </div>
    );
  }
}
export default App;
